// Soal 1
console.log('---------------------SOAL 1------------------------');

const arrayToObject = arr => {
  if (arr.length === 0) {
    console.log('');
  }

  const obj = {
    firstName: '',
    lastName: '',
    gender: '',
    age: ''
  };

  let now = new Date();
  let thisYear = now.getFullYear();

  for (let i in arr) {
    obj.firstName = arr[i][0];
    obj.lastName = arr[i][1];
    obj.gender = arr[i][2];
    arr[i][3] !== undefined && arr[i][3] < thisYear
      ? (obj.age = thisYear - arr[i][3])
      : (obj.age = 'Invalid Birth Year');

    console.log(`${++i} - ${obj.firstName} ${obj.lastName} : `, obj);
  }
};

var people = [
  ['Bruce', 'Banner', 'male', 1975],
  ['Natasha', 'Romanoff', 'female']
];

var people2 = [
  ['Tony', 'Stark', 'male', 1980],
  ['Pepper', 'Pots', 'female', 2023]
];

arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);

// Soal 2
console.log('---------------------SOAL 2------------------------');

const shoppingTime = (memberID, money) => {
  let changeMoney = money;
  let listPurchased = [];

  const products = {
    'Sepatu Stacattu': 1500000,
    'Baju Zoro': 500000,
    'Baju H&N': 250000,
    'Sweater Unikloh': 175000,
    'Casing Handphone': 50000
  };

  for (let i in products) {
    if (changeMoney >= products[i]) {
      changeMoney -= products[i];

      listPurchased.push(i);
    }
  }

  const data = {
    memberID: memberID,
    money: money,
    listPurchased: listPurchased,
    changeMoney: changeMoney
  };

  if (memberID === '' || memberID === undefined) {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  } else if (money < 50000) {
    return 'Mohon maaf, uang tidak cukup';
  } else {
    return data;
  }
};

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
console.log('---------------------SOAL 3------------------------');

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  const output = [];

  for (let i in arrPenumpang) {
    const penumpang = arrPenumpang[i][0];
    const naikDari = arrPenumpang[i][1];
    const tujuan = arrPenumpang[i][2];

    for (let j in rute) {
      if (naikDari === rute[j]) {
        var start = j;
      }

      if (tujuan === rute[j]) {
        var finish = j;
      }
    }

    const bayar = (finish - start) * 2000;

    const result = {
      penumpang: penumpang,
      naikDari: naikDari,
      tujuan: tujuan,
      bayar: bayar
    };

    output.push(result);
  }

  return output;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]
