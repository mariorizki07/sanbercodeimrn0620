// Soal 1
console.log('-------------SOAL 1--------------------');

const teriak = () => 'Halo Sanbers!';

console.log(teriak()); // "Halo Sanbers!"

// Soal 2
console.log('-------------SOAL 2--------------------');

const kalikan = (num1, num2) => num1 * num2;

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

// Soal 3
console.log('-------------SOAL 3--------------------');

const introduce = (name, age, address, hobby) =>
  `Nama saya ${name}, umur saya ${age}, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`;

var name = 'Agus';
var age = 30;
var address = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"
