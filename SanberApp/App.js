import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import YouTubeUI from './Tugas/Tugas12/App';
// import Tugas13 from './Tugas/Tugas13/App';
import TodoApp from './Tugas/Tugas14/App';
import SkillScreen from './Tugas/Tugas14/SkillScreen';

export default function App() {
  return <SkillScreen />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
