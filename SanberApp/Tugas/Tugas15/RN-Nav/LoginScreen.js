import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image source={require('./images/logo.png')} style={styles.logo} />
        </View>
        <View>
          <Text style={styles.loginText}>Login</Text>
        </View>
        <View style={{ padding: 10 }}>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Username</Text>
            <TextInput Label="Username" style={styles.inputs} />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Password</Text>
            <TextInput Label="Password" style={styles.inputs} />
          </View>
        </View>
        <View style={{ padding: 10 }}>
          <TouchableOpacity
            style={styles.button1}
            onPress={() => this.props.navigation.navigate('Logout')}
          >
            <Text style={{ fontSize: 18, color: 'white' }}> Masuk </Text>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{ fontSize: 18, color: '#3EC6FF' }}>atau</Text>
        </View>
        <View style={{ padding: 10 }}>
          <TouchableOpacity
            style={styles.button2}
            onPress={() => this.onPress()}
          >
            <Text style={{ fontSize: 18, color: 'white' }}> Daftar ? </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20
  },
  logo: {
    width: 300,
    height: 100
  },
  loginText: {
    fontSize: 25,
    marginTop: 50,
    color: '#3c3c3c'
  },
  inputLabel: {
    fontSize: 15
  },
  inputWrapper: {
    padding: 10,
    flexDirection: 'column',
    alignItems: 'stretch',
    marginTop: 10,
    marginBottom: 10
  },
  inputs: {
    height: 35,
    padding: 5,
    borderWidth: 2,
    borderColor: 'grey',
    backgroundColor: '#ffffff'
  },
  button1: {
    alignItems: 'center',
    backgroundColor: '#3EC6FF',
    borderRadius: 15,
    padding: 10
  },
  button2: {
    alignItems: 'center',
    backgroundColor: '#003366',
    borderRadius: 15,
    padding: 10
  }
});
