import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.judulPage}>
          <Text style={styles.judulPage}>Tentang Saya</Text>
        </View>
        <View>
          <Icon style={styles.pp} name="account-circle" size={15} />
        </View>
        <View style={{ alignItems: 'center', marginTop: 10 }}>
          <Text style={styles.judulPage}>Zahrony D Martanto</Text>
          <Text style={{ color: '#3EC6FF', fontSize: 18 }}>
            React Native Developer
          </Text>
        </View>
        <View style={styles.ctn}>
          <Text style={{ fontSize: 20, marginTop: 10 }}>Portofolio</Text>
          <View
            style={{
              borderBottomColor: '#999999',
              borderBottomWidth: 1,
              paddingVertical: 5,
              width: '100%'
            }}
          />

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View style={{ paddingHorizontal: 15 }}>
              <Icon name="gitlab" size={50} />
              <Text style={{ fontSize: 18, marginTop: 10 }}>@zacherony</Text>
            </View>
            <View style={{ paddingHorizontal: 15 }}>
              <Icon name="github-circle" size={50} />
              <Text style={{ fontSize: 18, marginTop: 10 }}>@zahrony</Text>
            </View>
          </View>
        </View>
        <View style={styles.ctn}>
          <Text style={{ fontSize: 20, marginTop: 10 }}>Hubungi Saya</Text>
          <View
            style={{
              borderBottomColor: '#999999',
              borderBottomWidth: 1,
              paddingVertical: 5,
              width: '100%'
            }}
          />

          <View style={{ justifyContent: 'Space-evenly' }}>
            <View style={styles.itemSocial}>
              <Icon name="instagram" size={40} />
              <Text style={{ fontSize: 18, marginTop: 20 }}>@zach</Text>
            </View>
            <View style={styles.itemSocial}>
              <Icon name="facebook" size={40} />
              <Text style={{ fontSize: 18, marginTop: 20 }}>@zach</Text>
            </View>
            <View style={styles.itemSocial}>
              <Icon name="twitter" size={40} />
              <Text style={{ fontSize: 18, marginTop: 20 }}>@zach</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  judulPage: {
    fontSize: 25
  },
  pp: {
    marginTop: 10,
    color: '#dddddd',
    fontSize: 150
  },
  ctn: {
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#EFEFEF',
    marginVertical: 5,
    padding: 10,
    borderRadius: 5
  },
  itemSocial: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15
  }
});
