import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import data from './skillData.json';

export default class SkillScreen extends Component {
  renderItem(skillData) {
    return (
      <View key={skillData.id} style={styles.listItem}>
        <Icon style={styles.listIcon} name={skillData.logoUrl} size={100} />
        <View style={styles.listDesc}>
          <Text style={styles.skillName}>{skillData.skillName}</Text>
          <Text style={styles.categoryName}>{skillData.categoryName}</Text>
          <Text style={styles.percentageProgress}>
            {skillData.percentageProgress}
          </Text>
        </View>
        <Icon style={styles.listIcon} name="chevron-right" size={100} />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto" />
        <Image source={require('./images/logo.png')} style={styles.logo} />
        <View style={styles.profilePicture}>
          <Icon style={styles.loginIcon} name="account-circle" size={40} />
          <View style={styles.profileText}>
            <Text style={{ color: '#003366' }}>Hai,</Text>
            <Text style={{ color: '#003366', fontSize: 16 }}>Mario Rizki</Text>
          </View>
        </View>
        <View style={styles.skillHeader}>
          <Text style={{ color: '#003366', fontSize: 36 }}>SKILL</Text>
        </View>
        <View style={styles.category}>
          <Text style={styles.categotyText}>Library / Framework</Text>
          <Text style={styles.categotyText}>Bahasa Pemrograman</Text>
          <Text style={styles.categotyText}>Teknologi</Text>
        </View>
        <FlatList
          style={{ width: '100%' }}
          data={data.items}
          renderItem={skillData => this.renderItem(skillData.item)}
          key={item => item.id}
          ItemSeparatorComponent={() => {
            return <View style={{ height: 10 }} />;
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    padding: 15,
    paddingTop: 0,
    alignItems: 'flex-start'
  },
  logo: {
    width: 200,
    height: 70,
    resizeMode: 'cover',
    alignSelf: 'flex-end'
  },
  profilePicture: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  loginIcon: {
    color: '#3EC6FF',
    marginRight: 10
  },
  profileText: {
    color: '#003366'
  },
  skillHeader: {
    width: '100%',
    marginTop: 10,
    borderBottomWidth: 4,
    borderColor: '#3EC6FF'
  },
  category: {
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  categotyText: {
    height: 32,
    padding: 5,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    color: '#003366',
    fontSize: 14,
    fontWeight: 'bold'
  },
  listItem: {
    width: '100%',
    height: 140,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 8,
    elevation: 2
  },
  listIcon: {
    color: '#003366'
  },
  listDesc: {
    width: 170,
    marginLeft: 15
  },
  skillName: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366'
  },
  categoryName: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3EC6FF'
  },
  percentageProgress: {
    alignSelf: 'flex-end',
    fontSize: 48,
    fontWeight: 'bold',
    color: '#fff'
  }
});
