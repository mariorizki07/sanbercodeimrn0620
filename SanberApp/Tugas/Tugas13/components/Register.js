import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../images/logo.png')}></Image>
        <View>
          <Text style={styles.loginText}>Register</Text>
        </View>
        <View style={{ marginTop: -10 }}>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Username</Text>
            <TextInput placeholder="Username" style={styles.inputs} />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Username</Text>
            <TextInput placeholder="Username" style={styles.inputs} />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Ulangi Password</Text>
            <TextInput
              placeholder="Password"
              secureTextEntry
              style={styles.inputs}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Text style={styles.inputLabel}>Password</Text>
            <TextInput
              placeholder="Password"
              secureTextEntry
              style={styles.inputs}
            />
          </View>
        </View>
        <View style={{ marginTop: -10 }}>
          <TouchableOpacity style={styles.registerButton}>
            <Text style={{ fontSize: 20, color: 'white' }}> Daftar </Text>
          </TouchableOpacity>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#3EC6FF' }}>
            atau
          </Text>
          <TouchableOpacity style={styles.loginButton}>
            <Text style={{ fontSize: 20, color: 'white' }}> Masuk ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  loginText: {
    color: '#003366',
    fontSize: 20
  },
  inputWrapper: {
    padding: 10,
    width: 200
  },
  inputLabel: {
    fontSize: 15,
    color: '#003366'
  },
  inputs: {
    height: 35,
    padding: 5,
    borderWidth: 2,
    borderColor: '#003366',
    backgroundColor: '#ffffff'
  },
  loginButton: {
    backgroundColor: '#3EC6FF',
    borderRadius: 15,
    padding: 10,
    alignItems: 'center',
    marginBottom: 10
  },
  registerButton: {
    alignItems: 'center',
    backgroundColor: '#003366',
    borderRadius: 15,
    padding: 10,
    alignItems: 'center',
    marginTop: 10
  }
});

export default App;
