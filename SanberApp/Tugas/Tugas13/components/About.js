import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class About extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.text}>Tentang Saya</Text>
        </View>
        <View>
          <Icon style={styles.profilePicture} name="account-circle" size={15} />
        </View>
        <View style={{ alignItems: 'center', marginTop: 10 }}>
          <Text style={styles.text}>Mario Rizki</Text>
          <Text style={{ color: '#3EC6FF', fontSize: 18 }}>
            React Native Developer
          </Text>
        </View>
        <View style={styles.layout}>
          <Text style={{ fontSize: 20, marginTop: 10 }}>Portofolio</Text>
          <View
            style={{
              borderBottomColor: '#999999',
              borderBottomWidth: 1,
              paddingVertical: 5,
              width: '100%'
            }}
          />

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View style={{ paddingHorizontal: 15 }}>
              <Icon name="gitlab" size={50} color="#3EC6FF" />
              <Text style={{ fontSize: 18, marginTop: 10, color: '#3EC6FF' }}>
                @mariorizki07
              </Text>
            </View>
            <View style={{ paddingHorizontal: 15 }}>
              <Icon name="github-circle" size={50} color="#3EC6FF" />
              <Text style={{ fontSize: 18, marginTop: 10, color: '#3EC6FF' }}>
                @mariorizki
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.layout}>
          <Text
            style={{
              fontSize: 20,
              marginTop: 10
            }}
          >
            Hubungi Saya
          </Text>
        </View>
        <View style={{ justifyContent: 'space-evenly' }}>
          <View style={styles.socialMedia}>
            <Icon name="instagram" size={40} color="#3EC6FF" />
            <Text style={{ fontSize: 18, marginTop: 20, color: '#3EC6FF' }}>
              @mariorizki
            </Text>
          </View>
          <View style={styles.socialMedia}>
            <Icon name="facebook" size={40} color="#3EC6FF" />
            <Text style={{ fontSize: 18, marginTop: 20, color: '#3EC6FF' }}>
              Mario Rizki
            </Text>
          </View>
          <View style={styles.socialMedia}>
            <Icon name="twitter" size={40} color="#3EC6FF" />
            <Text style={{ fontSize: 18, marginTop: 20, color: '#3EC6FF' }}>
              @mariorizki
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  text: {
    fontSize: 25
  },
  profilePicture: {
    marginTop: 10,
    color: '#dddddd',
    fontSize: 150
  },
  layout: {
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#EFEFEF',
    marginVertical: 5,
    padding: 10,
    borderRadius: 5
  },
  socialMedia: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15
  }
});

export default About;
