let a = 1;
console.log('LOOPING PERTAMA');

while (a <= 20) {
  if (a % 2 === 0) {
    console.log(`${a} - I love coding`);
  }
  a++;
}

let b = 20;
console.log('LOOPING KEDUA');

while (b >= 1) {
  if (b % 2 === 0) {
    console.log(`${b} - I will become a mobile developer`);
  }
  b--;
}

// Soal 2
console.log('--------SOAL 2-----------');

for (let i = 1; i <= 20; i++) {
  if (i % 3 === 0 && i % 2 !== 0) {
    console.log(`${i} - I love coding`);
  } else if (i % 2 === 0) {
    console.log(`${i} - Berkualitas`);
  } else {
    console.log(`${i} - Santai`);
  }
}

// Soal 3
console.log('--------SOAL 3-----------');

for (let i = 1; i <= 5; i++) {
  let hash = '';
  for (let j = 1; j <= 5; j++) {
    hash += '#';
  }
  console.log(hash);
}

//  Soal 4
console.log('--------SOAL 4-----------');

for (let i = 1; i <= 7; i++) {
  let hash = '';
  for (let j = 1; j <= 7; j++) {
    if (i >= j) {
      hash += '#';
    }
  }
  console.log(hash);
}

// Soal 5
console.log('--------SOAL 5-----------');

for (let i = 1; i <= 8; i++) {
  let hash = '';
  if (i % 2 === 0) {
    for (let j = 1; j <= 8; j++) {
      if (j % 2 === 0) {
        hash += ' ';
      } else {
        hash += '#';
      }
    }
  } else {
    for (let j = 1; j <= 8; j++) {
      if (j % 2 === 0) {
        hash += '#';
      } else {
        hash += ' ';
      }
    }
  }
  console.log(hash);
}
