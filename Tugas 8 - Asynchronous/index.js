var readBooks = require('./callback.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
];

// Tulis code untuk memanggil function readBooks di sini
let i = 0;
let timeAvailable = 10000;
const read = (timeAvailable, books) => {
  if (i < books.length) {
    readBooks(timeAvailable, books[i], () => {
      read(timeAvailable, books);
    });
    timeAvailable -= books[i].timeSpent;
    i++;
  }
};

read(timeAvailable, books);
