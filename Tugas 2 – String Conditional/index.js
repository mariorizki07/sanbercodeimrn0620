// Soal 1
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

console.log(
  word +
    ' ' +
    second +
    ' ' +
    third +
    ' ' +
    fourth +
    ' ' +
    fifth +
    ' ' +
    sixth +
    ' ' +
    seventh
);

// Soal 2
var sentence = 'I am going to be React Native Developer';

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence.substr(5, 5); // lakukan sendiri
var fourthWord = sentence.substr(11, 2); // lakukan sendiri
var fifthWord = sentence.substr(14, 2); // lakukan sendiri
var sixthWord = sentence.substr(17, 5); // lakukan sendiri
var seventhWord = sentence.substr(23, 7); // lakukan sendiri
var eighthWord = sentence.substr(30, 9); // lakukan sendiri

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord);

// Soal 3
var sentence2 = 'wow JavaScript is so cool';

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14); // do your own!
var thirdWord2 = sentence2.substring(15, 17); // do your own!
var fourthWord2 = sentence2.substring(18, 20); // do your own!
var fifthWord2 = sentence2.substring(21, 25); // do your own!

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

// Soal 4
var sentence3 = 'wow JavaScript is so cool';

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14); // do your own!
var thirdWord3 = sentence3.substring(15, 17); // do your own!
var fourthWord3 = sentence3.substring(18, 20); // do your own!
var fifthWord3 = sentence3.substring(21, 25); // do your own!

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;
// lanjutkan buat variable lagi di bawah ini
console.log(
  'First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength
);
console.log(
  'Second Word: ' + secondWord3 + ', with length: ' + secondWordLength
);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);
console.log(
  'Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength
);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);

// Soal if-else
var nama = 'John';
var peran = 'Werewolf'; // Penyihir, Guard, Werewolf

if (nama === '') {
  console.log('Nama harus diisi');
} else if (peran === '') {
  console.log(`Halo ${nama}, Pilih peranmu untuk memulai game`);
} else if (nama !== '' && peran === 'Penyihir') {
  console.log(
    `Selamat datang di Dunia Werewolf, ${nama} \nHalo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`
  );
} else if (nama !== '' && peran === 'Guard') {
  console.log(
    `Selamat datang di Dunia Werewolf, ${nama} \nHalo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`
  );
} else if (nama !== '' && peran === 'Werewolf') {
  console.log(
    `Selamat datang di Dunia Werewolf, ${nama} \nHalo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`
  );
} else {
  console.log('invalid role input');
}

// Soal switch-case
var tanggal = 24; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) {
  case 1:
    console.log(tanggal + ' ' + 'Januari' + ' ' + tahun);
    break;
  case 2:
    console.log(tanggal + ' ' + 'Februari' + ' ' + tahun);
    break;
  case 3:
    console.log(tanggal + ' ' + 'Maret' + ' ' + tahun);
    break;
  case 4:
    console.log(tanggal + ' ' + 'April' + ' ' + tahun);
    break;
  case 5:
    console.log(tanggal + ' ' + 'Mei' + ' ' + tahun);
    break;
  case 6:
    console.log(tanggal + ' ' + 'Juni' + ' ' + tahun);
    break;
  case 7:
    console.log(tanggal + ' ' + 'Juli' + ' ' + tahun);
    break;
  case 8:
    console.log(tanggal + ' ' + 'Agustus' + ' ' + tahun);
    break;
  case 9:
    console.log(tanggal + ' ' + 'September' + ' ' + tahun);
    break;
  case 10:
    console.log(tanggal + ' ' + 'Oktober' + ' ' + tahun);
    break;
  case 11:
    console.log(tanggal + ' ' + 'November' + ' ' + tahun);
    break;
  case 12:
    console.log(tanggal + ' ' + 'Desember' + ' ' + tahun);
    break;
  default:
    console.log('invalid month input');
    break;
}
