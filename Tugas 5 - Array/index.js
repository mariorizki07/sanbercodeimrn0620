// Soal 1
const range = (startNum, finishNum) => {
  if (startNum < finishNum) {
    const result = [];
    for (let i = startNum; i <= finishNum; i++) {
      result.push(i);
    }
    return result;
  } else if (startNum > finishNum) {
    const result = [];
    for (let i = startNum; i >= finishNum; i--) {
      result.push(i);
    }
    return result;
  } else {
    return -1;
  }
};

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// Soal 2
console.log('-----------------SOAL 2-------------');

const rangeWithStep = (startNum, finishNum, step) => {
  if (startNum < finishNum) {
    const result = [];
    for (let i = startNum; i <= finishNum; i += step) {
      result.push(i);
    }
    return result;
  } else {
    const result = [];
    for (let i = startNum; i >= finishNum; i -= step) {
      result.push(i);
    }
    return result;
  }
};

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// soal 3
console.log('-----------------SOAL 3-------------');

const sum = (num1, num2, step) => {
  const temp = [];
  let result = 0;

  if (step === undefined && num2 !== undefined) {
    if (num1 < num2) {
      for (let i = num1; i <= num2; i++) {
        temp.push(i);
      }
    } else {
      for (let i = num1; i >= num2; i--) {
        temp.push(i);
      }
    }
  } else if (step !== undefined) {
    if (num1 < num2) {
      for (let i = num1; i <= num2; i += step) {
        temp.push(i);
      }
    } else {
      for (let i = num1; i >= num2; i -= step) {
        temp.push(i);
      }
    }
  } else if (num2 === undefined && num1 !== undefined) {
    return num1;
  } else {
    return 0;
  }

  for (i = 0; i < temp.length; i++) {
    result += temp[i];
  }

  return result;
};





// const sum = (num1, num2, step) => {
//   if (step === undefined) {
//     const temp = range(num1, num2);
//     let result = 0;
//     for (i = 0; i < temp.length; i++) {
//       result += temp[i];
//     }
//     return result;
//   } else if (step !== undefined) {
//     const temp = rangeWithStep(num1, num2, step);
//     let result = 0;
//     for (let i = 0; i < temp.length; i++) {
//       result += temp[i];
//     }
//     return result;
//   }
// };

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// Soal 4
console.log('-----------------SOAL 4-------------');

var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun']
];

const dataHandling = arr => {
  for (let i in arr) {
    console.log(`Nomor ID: ${arr[i][0]}`);
    console.log(`Nama Lengkap: ${arr[i][1]}`);
    console.log(`TTL: ${arr[i][2]}`);
    console.log(`Hobi: ${arr[i][3]}`);
    console.log('');
  }
};

dataHandling(input);

// Soal 5
console.log('-----------------SOAL 5-------------');

const balikKata = str => {
  let result = '';
  for (let i = str.length - 1; i >= 0; i--) {
    result += str[i];
  }

  return result;
};

console.log(balikKata('Kasur Rusak')); // kasuR rusaK
console.log(balikKata('SanberCode')); // edoCrebnaS
console.log(balikKata('Haji Ijah')); // hajI ijaH
console.log(balikKata('racecar')); // racecar
console.log(balikKata('I am Sanbers')); // srebnaS ma I

// Soal 6
console.log('-----------------SOAL 6-------------');

const dataHandling2 = () => {
  var input = [
    '0001',
    'Roman Alamsyah',
    'Bandar Lampung',
    '21/05/1989',
    'Membaca'
  ];

  var nama = input[1].split(' ');
  var provinsi = input[2].split(' ');

  nama.push('Elsharawy');
  provinsi.unshift('Provinsi');

  input.splice(1, 1, nama.join(' '));
  input.splice(2, 1, provinsi.join(' '));
  input.splice(4, 0, 'Pria', 'SMA Internasional Metro');

  var deleteSlash = input[3].split('/');
  var bulan = Number(deleteSlash[1]);
  var gabung = deleteSlash.join('-');

  deleteSlash.sort(function(value1, value2) {
    return Number(value1) < Number(value2);
  });

  switch (bulan) {
    case (bulan = 1):
      bulan = 'Januari';
      break;
    case (bulan = 2):
      bulan = 'Februari';
      break;
    case (bulan = 3):
      bulan = 'Maret';
      break;
    case (bulan = 4):
      bulan = 'April';
      break;
    case (bulan = 5):
      bulan = 'Mei';
      break;
    case (bulan = 6):
      bulan = 'Juni';
      break;
    case (bulan = 7):
      bulan = 'Juli';
      break;
    case (bulan = 8):
      bulan = 'Agustus';
      break;
    case (bulan = 9):
      bulan = 'September';
      break;
    case (bulan = 10):
      bulan = 'Oktober';
      break;
    case (bulan = 11):
      bulan = 'November';
      break;
    case (bulan = 12):
      bulan = 'Desember';
      break;
  }

  console.log(input);
  console.log(bulan);
  console.log(deleteSlash);
  console.log(gabung);
  console.log(input[1].slice(0, 15));
};

dataHandling2();
